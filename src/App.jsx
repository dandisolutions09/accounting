

import { HashRouter, Route, Routes } from 'react-router-dom'
import Dashboard from './pages/Dashboard'
import Navbar from './components/Navbar';

import { ThemeProvider, createTheme } from '@mui/material';
import TrialsPage from './pages/TrialsPage';

function App() {
  const theme = createTheme({
    typography: {
      fontFamily: ["Play"],
    },
  });

  return (
    <>
      <ThemeProvider theme={theme}>
        <HashRouter>
          <Navbar />
          <Routes>
            <Route path="/" element={<Dashboard />} />
            <Route path="/trials" element={<TrialsPage />} />
          </Routes>
        </HashRouter>
      </ThemeProvider>
    </>
  );
}

export default App
