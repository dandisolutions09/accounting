import React from "react";
import { Chart } from "react-google-charts";

export const data = [
  ["Task", "Hours per Day"],
  ["Furniture & Fitting", 11],
  ["Legal Fees", 2],
  ["Prepaid Expenses", 2],
  ["Utilities", 2],
  ["Miscellaneous", 7],
];

export const options = {
  title: "Joshua Odhiambo Nyamori Advocate",
  is3D: true,
};

export function ChartPie() {
  return (
    <Chart
      chartType="PieChart"
      data={data}
      options={options}
      width={"100%"}
      height={"400px"}
    />
  );
}
