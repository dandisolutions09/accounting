import React from "react";
import { Chart } from "react-google-charts";

export const data = [
  ["City", "Revenue", "Expenses"],
  ["Jan", 8175000, 8008000],
  ["Feb", 3792000, 3694000],
  ["Mar", 2695000, 2896000],
  ["Apr", 2099000, 1953000],
  ["May", 1526000, 1517000],
  ["June", 1526000, 1517000],
  ["July", 1526000, 1517000],
  ["Aug", 1526000, 1517000],
  ["Sept", 1526000, 1517000],
  ["Oct", 1526000, 1517000],
  ["Nov", 1526000, 1517000],
  ["Dec", 1526000, 1517000],
];

export const options = {
  title: "Population of Largest U.S. Cities",

  chartArea: { width: "50%" },
  hAxis: {
    title: "Total Population",
    minValue: 0,
  },
  vAxis: {
    title: "Month",
  },
  is3D: true,
};

export function BarChartz() {
  return (
    <Chart
      chartType="Bar"
      width="100%"
      height="400px"
      data={data}
      options={options}
    />
  );
}
