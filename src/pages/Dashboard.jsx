import React from 'react'
import { ChartPie } from '../components/ChartPie'
import { BarChartz } from '../components/BarChartz';
import { Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';


export default function Dashboard() {
    const navigate = useNavigate();
    const handleNavigate_trials = () =>{
        navigate("/trials")
    }
  return (
    <div>
      <div className="absolute right-4 top-[76px] z-20">
        <Button variant="contained" onClick={handleNavigate_trials}>
          Trial Balance
        </Button>
      </div>
      <ChartPie />
      <BarChartz />
    </div>
  );
}
